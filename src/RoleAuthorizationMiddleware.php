<?php

namespace Glance\KeycloakMiddleware;

use Exception;
use Glance\CernAuthentication\User;
use Glance\KeycloakMiddleware\Exception\PermissionDeniedException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Role Authorization Middleware
 *
 * @author Mario Simao <mario.simao@cern.ch>
 */
class RoleAuthorizationMiddleware implements MiddlewareInterface
{
    /** @var array<string|string[]> */
    private $requirements;

    /** @param array<string|string[]> $requirements */
    private function __construct(array $requirements)
    {
        $this->requirements = $requirements;
    }

    /** @param string[] $roles */
    public static function anyOfRoles(array $roles): self
    {
        return new self($roles);
    }

    /** @param string[] $roles */
    public static function allOfRoles(array $roles): self
    {
        return new self([ $roles ]);
    }

    public static function mustHaveRole(string $role): self
    {
        return new self([ $role ]);
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $user = $request->getAttribute("keycloak-user");

        if ($user === null) {
            throw new Exception(
                "Missing 'keycloak-user' attribute. Is this an authenticated route?"
            );
        }

        if (is_object($user) === false || get_class($user) !== User::class) {
            $userClass = User::class;
            throw new Exception(
                "Attribute 'keycloak-user' must be of the class: '{$userClass}'."
            );
        }

        $authorized = $this->authorize($user->roles());
        if ($authorized === false) {
            throw new PermissionDeniedException();
        }

        return $handler->handle($request);
    }

    /** @param string[] $roles */
    public function authorize(array $roles): bool
    {
        $requirements = $this->requirements;

        if (empty($requirements)) {
            return true;
        }

        foreach ($requirements as $requirement) {
            if (is_array($requirement)) {
                if (self::hasAllRoles($roles, $requirement)) {
                    return true;
                }
            } else {
                if (self::hasRole($roles, $requirement)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param string[] $userRoles
     * @param string[] $requiredRoles
     */
    private static function hasAllRoles(array $userRoles, array $requiredRoles): bool
    {
        foreach ($requiredRoles as $requiredRole) {
            if (self::hasRole($userRoles, $requiredRole) === false) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string[] $userRoles
     */
    private static function hasRole(array $userRoles, string $requiredRole): bool
    {
        return in_array($requiredRole, $userRoles);
    }
}
