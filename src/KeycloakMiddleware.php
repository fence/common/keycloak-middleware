<?php

namespace Glance\KeycloakMiddleware;

use Glance\CernAuthentication\Exception\FailedToDecodeTokenException;
use Glance\CernAuthentication\Exception\InactiveTokenException;
use Glance\CernAuthentication\KeycloakProvider;
use Glance\KeycloakMiddleware\Exception\ExpiredTokenException;
use Glance\KeycloakMiddleware\Exception\InsecureRequestException;
use Glance\KeycloakMiddleware\Exception\InvalidTokenException;
use Glance\KeycloakMiddleware\Exception\TokenNotFoundException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Keycloak Middleware
 *
 * @author Mario Simao <mario.simao@cern.ch>
 */
class KeycloakMiddleware implements MiddlewareInterface
{
    /** @var string */
    private $clientId;

    /** @var string */
    private $clientSecret;

    /** @var string[] */
    private $paths;

    /** @var string[] */
    private $passThrough;

    /** @var KeycloakProvider */
    private $keycloakProvider;

    /** @var callable|null */
    private $afterAuthenticate;

    /**
     * @param string[] $paths
     * @param string[] $passThrough
     */
    private function __construct(
        string $clientId,
        string $clientSecret,
        array $paths,
        array $passThrough,
        KeycloakProvider $keycloakProvider,
        ?callable $afterAuthenticate
    ) {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->paths = $paths;
        $this->passThrough = $passThrough;
        $this->keycloakProvider = $keycloakProvider;
        $this->afterAuthenticate = $afterAuthenticate;
    }

    /**
     * @param string[] $paths
     * @param string[] $passThrough
     */
    public static function create(
        string $clientId,
        string $clientSecret,
        array $paths = ["/"],
        array $passThrough = [],
        KeycloakProvider $keycloakProvider = null,
        callable $afterAuthenticate = null
    ): self {
        if ($keycloakProvider === null) {
            $keycloakProvider = KeycloakProvider::create();
        }

        return new self(
            $clientId,
            $clientSecret,
            $paths,
            $passThrough,
            $keycloakProvider,
            $afterAuthenticate
        );
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $this->checkHttps($request);

        if (!$this->shouldAuthenticate($request)) {
            return $handler->handle($request);
        }

        $token = $this->fetchToken($request);
        $user = $this->createUser($token);

        if ($this->afterAuthenticate !== null) {
            $afterAuthenticate = $this->afterAuthenticate;
            $afterAuthenticate($request, $user);
        }

        return $handler->handle($request->withAttribute("keycloak-user", $user));
    }

    /**
     * Checks if the current endpoint should be authenticated.
     *
     * First, checks on the 'passThrough' option and then on the 'path' option.
     *
     * The regexp were copied from the 1st version of slim-jwt-auth.
     */
    private function shouldAuthenticate(ServerRequestInterface $request): bool
    {
        $uri = $request->getUri()->getPath();
        $uri = preg_replace("#/+#", "/", $uri);

        /**
         * If request path is matches passThrough should not authenticate.
         * https://github.com/tuupola/slim-jwt-auth/blob/1.x/src/JwtAuthentication/RequestPathRule.php#L38
         */
        foreach ($this->passThrough as $passThrough) {
            $passThrough = rtrim($passThrough, "/");
            if (!!preg_match("@^{$passThrough}(/.*)?$@", $uri)) {
                return false;
            }
        }

        /**
         * Otherwise check if path matches and we should authenticate.
         * https://github.com/tuupola/slim-jwt-auth/blob/1.x/src/JwtAuthentication/RequestPathRule.php#L46
         */
        foreach ($this->paths as $path) {
            $path = rtrim($path, "/");
            if (!!preg_match("@^{$path}(/.*)?$@", $uri)) {
                return true;
            }
        }

        return false;
    }

    private function checkHttps(ServerRequestInterface $request): void
    {
        $host = $request->getUri()->getHost();
        if ($host === "localhost" || $host === "127.0.0.1") {
            return;
        }

        $scheme = $request->getUri()->getScheme();
        if ($scheme !== "https") {
            throw new InsecureRequestException();
        }
    }

    private function fetchToken(ServerRequestInterface $request): string
    {
        $authorizationHeaders = $request->getHeader("Authorization");

        if (count($authorizationHeaders) === 0) {
            throw new TokenNotFoundException();
        }

        $authorizationHeader = array_pop($authorizationHeaders);

        if (preg_match("/Bearer\s+(.*)$/i", $authorizationHeader, $matches)) {
            return $matches[1];
        }

        throw new TokenNotFoundException();
    }

    /**
     * @return \Glance\CernAuthentication\User|\Glance\CernAuthentication\ApiUser
     */
    private function createUser(string $token)
    {
        try {
            return $this->keycloakProvider->createUser(
                $token,
                $this->clientId
            );
        } catch (FailedToDecodeTokenException $e) {
            throw new InvalidTokenException();
        } catch (InactiveTokenException $e) {
            throw new ExpiredTokenException();
        }
    }

    public function clientId(): string
    {
        return $this->clientId;
    }

    public function clientSecret(): string
    {
        return $this->clientSecret;
    }

    public function authenticatedPaths(): array
    {
        return $this->paths;
    }

    public function passThroughPaths(): array
    {
        return $this->passThrough;
    }
}
