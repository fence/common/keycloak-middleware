<?php

namespace Glance\KeycloakMiddleware\Exception;

use Glance\ErrorMiddleware\BaseException;
use Glance\ErrorMiddleware\Error;

/**
 * PermissionDeniedException
 *
 * @author Mario Simão <mario.simao@cern.ch>
 */
class PermissionDeniedException extends BaseException
{
    public function __construct()
    {
        $status = 403;
        $title = "Permission denied";
        $detail = "You are not allowed to access this endpoint.";

        $error = new Error();
        $error->setStatus($status)->setTitle($title)->setDetail($detail);

        parent::__construct($status, [$error]);
    }
}
