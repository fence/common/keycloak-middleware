<?php

namespace Glance\KeycloakMiddleware\Exception;

use Glance\ErrorMiddleware\BaseException;
use Glance\ErrorMiddleware\Error;

/**
 * TokenNotFoundException
 *
 * @author Mario Simão <mario.simao@cern.ch>
 */
class TokenNotFoundException extends BaseException
{
    public function __construct()
    {
        $status = 401;
        $title = "Invalid authentication token.";
        $detail = "Authentication token not found.";

        $error = new Error();
        $error->setStatus($status)->setTitle($title)->setDetail($detail);

        parent::__construct($status, [$error]);
    }
}
