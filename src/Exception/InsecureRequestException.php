<?php

namespace Glance\KeycloakMiddleware\Exception;

use Glance\ErrorMiddleware\BaseException;
use Glance\ErrorMiddleware\Error;

/**
 * InvalidRequestBodyException
 *
 * @author Mario Simão <mario.simao@cern.ch>
 */
class InsecureRequestException extends BaseException
{
    public function __construct()
    {
        $status = 401;
        $title = "Invalid scheme.";
        $detail = "Insecure request. Please use HTTPS.";

        $error = new Error();
        $error->setStatus($status)->setTitle($title)->setDetail($detail);

        parent::__construct($status, [$error]);
    }
}
