# Keycloak Middleware

[PSR-15](https://www.php-fig.org/psr/psr-15/) middleware for CERN Keycloak
authentication.

## Installation

Install using Composer:

```bash
composer require glance-project/keycloak-middleware
```

If using Apache add the following to the `.htaccess` file. Otherwise PHP wont have access to Authorization: Bearer header.

```
RewriteRule .* - [env=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
```

## Getting started

To use this library, you will need to have an application registered on
[Application Portal](https://application-portal.web.cern.ch). If you do not
know what this mean, refer to the [CERN Authorization Service documentation](https://auth.docs.cern.ch/applications/application-configuration)

With your application registered, you will need the **Client ID** and
**Client Secret**.

## Usage

This middleware can be used with any framework compatible with PSR-7 and PSR-15.
On the following examples, Slim will be used.

There are 2 mandatory arguments for the middleware creation: *Client ID* and
*Client Secret*.

```php
<?php

use Glance\KeycloakMiddleware\KeycloakMiddleware;

$app = new \Slim\App();

$keycloakMiddleware = KeycloakMiddleware::create(
    getenv("CLIENT_ID"),
    getenv("CLIENT_SECRET")
);

$app->add($keycloakMiddleware);
```

After authentication, the user will be injected to the request. You can access it via the
`keycloak-user` attribute.

```php
$user = $request->getAttribute("keycloak-user");
$personId = $user->personId();
$body = "User Person ID: {$personId}";

$response->getBody()->write($body);
```

### Paths

The optional `paths` parameter allows you to specify the protected part of your
API. You do not need to specify each URL. Instead think of path setting as a
folder. In the example below everything starting with `/api/private` will be
authenticated. If you do not define `paths` all routes will be protected.

```php
<?php

use Glance\KeycloakMiddleware\KeycloakMiddleware;

$app = new \Slim\App();

$keycloakMiddleware = KeycloakMiddleware::create(
    getenv("CLIENT_ID"),
    getenv("CLIENT_SECRET"),
    ["/api/private"]
);

$app->add($keycloakMiddleware);
```

### Pass through

With optional `passThrough` parameter you can make exceptions to `paths`
parameter. In the example below everything starting with `/api` will be
authenticated with the exception of `/api/public` which will not be
authenticated.

```php
<?php

use Glance\KeycloakMiddleware\KeycloakMiddleware;

$app = new \Slim\App();

$keycloakMiddleware = KeycloakMiddleware::create(
    getenv("CLIENT_ID"),
    getenv("CLIENT_SECRET"),
    ["/api"],
    ["/api/public"]
);

$app->add($keycloakMiddleware);
```

### Keycloak Provider

Optionally, you can provide your own instance of
`Glance\CernAuthentication\KeycloakProvider` through the `create()` method.

```php
<?php

use Glance\KeycloakMiddleware\KeycloakMiddleware;

$app = new \Slim\App();

$keycloakProvider = KeycloakProvider(/* ... */);

$keycloakMiddleware = KeycloakMiddleware::create(
    getenv("CLIENT_ID"),
    getenv("CLIENT_SECRET"),
    ["/api"],
    [],
    $keycloakProvider
);

$app->add($keycloakMiddleware);
```

### After authenticate

Optionally, you can provide a function which is called after the user is authenticated. This is
useful for getting user information on the scope outside the middleware.

```php
<?php

use Glance\CernAuthentication\User;
use Glance\KeycloakMiddleware\KeycloakMiddleware;
use Psr\Http\Message\ServerRequestInterface;

$app = new \Slim\App();

$keycloakProvider = KeycloakProvider(/* ... */);

$keycloakMiddleware = KeycloakMiddleware::create(
    getenv("CLIENT_ID"),
    getenv("CLIENT_SECRET"),
    ["/api"],
    [],
    $keycloakProvider
    function (ServerRequestInterface $request, User $user) {
        echo $user->personId();
    }
);

$app->add($keycloakMiddleware);
```

## Security

Access are essentially passwords. You should treat them as such and you should
always use HTTPS. If the middleware detects insecure usage over HTTP it will
throw an `InsecureRequestException`. This rule is relaxed for requests on
`localhost` and `127.0.0.1`.

## Authorization

First, it is important to know the difference between *authentication* and
*authorization*.

> In simple terms, authentication is the process of verifying who a user is,
> while authorization is the process of verifying what they have access to.

For authentication, use the second middleware provided by this library:
`RoleAuthorizationMiddleware`. It checks the roles from the user. Roles are
defined on the [Application Portal](https://application-portal.web.cern.ch) and
can be linked to groups.

The authorization middleware can be added to specific routes:

```php
<?php

use Glance\KeycloakMiddleware\KeycloakMiddleware;
use Glance\KeycloakMiddleware\RoleAuthorizationMiddleware;

$app = new \Slim\App();

// Add authentication middleware
$keycloakMiddleware = KeycloakMiddleware::create(
    getenv("CLIENT_ID"),
    getenv("CLIENT_SECRET")
);
$app->add($keycloakMiddleware);

// Define routes and add authorization middleware
$app->get("/members", /* route logic */)
    ->add(RoleAuthorizationMiddleware::mustHaveRole("common-user"));

$app->run();
```

### Any of roles

Checks if the user has at least one of the defined roles. It uses the `OR`
operator.

```php
<?php

use Glance\KeycloakMiddleware\RoleAuthorizationMiddleware;

$app = new \Slim\App();

// Add authentication middleware here

// To access this route, user must be 'common-user' OR 'visitor'
$app->get("/members", /* route logic */)
    ->add(RoleAuthorizationMiddleware::anyOfRoles([ "common-user", "visitor" ]));
```

### All of roles

Checks if the user has all the defined roles. It uses the `AND` operator.

```php
<?php

use Glance\KeycloakMiddleware\KeycloakMiddleware;
use Glance\KeycloakMiddleware\RoleAuthorizationMiddleware;

$app = new \Slim\App();

// Add authentication middleware here

// To access this route, user must be 'team-leader' AND 'active-user'
$app->get("/members", /* route logic */)
    ->add(RoleAuthorizationMiddleware::allOfRoles([ "team-leader", "active-user" ]));
```

### Must have role

Checks if the user has an specific role.

```php
<?php

use Glance\KeycloakMiddleware\KeycloakMiddleware;
use Glance\KeycloakMiddleware\RoleAuthorizationMiddleware;

$app = new \Slim\App();

// Add authentication middleware here

// To access this route, user must be 'admin'
$app->get("/members", /* route logic */)
    ->add(RoleAuthorizationMiddleware::allOfRoles("admin"));
```

## Exceptions

If you use this library in combination with[Glance error handler middleware](https://gitlab.cern.ch/fence/common/error-handler),
the validation errors will automatically be converted to proper HTTP responses.

```php
<?php

use Glance\KeycloakMiddleware\KeycloakMiddleware;

$app = new \Slim\App();

$ErrorMiddleware = new \Glance\ErrorMiddleware\ErrorMiddleware(/* ... */);

$keycloakMiddleware = KeycloakMiddleware::create(
    getenv("CLIENT_ID"),
    getenv("CLIENT_SECRET")
);

$app->add($keycloakMiddleware);
$app->add($ErrorMiddleware);
```

An example of error response from the previous setup:

```json
{
    "errors": [
        {
            "status": 401,
            "title": "Invalid authentication token.",
            "detail": "Authentication token introspection failed."
        }
    ]
}
```
