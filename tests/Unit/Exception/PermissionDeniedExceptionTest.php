<?php

namespace Glance\KeycloakMiddleware\Unit\Exception;

use Glance\KeycloakMiddleware\Exception\PermissionDeniedException;
use PHPUnit\Framework\TestCase;

final class PermissionDeniedExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $e = new PermissionDeniedException();

        $errors = $e->getErrors();
        $error = $errors[0];

        $this->assertSame(403, $error->getStatus());
        $this->assertSame("Permission denied", $error->getTitle());
        $this->assertSame("You are not allowed to access this endpoint.", $error->getDetail());
    }
}
