<?php

namespace Glance\KeycloakMiddleware\Unit\Exception;

use Glance\KeycloakMiddleware\Exception\InvalidTokenException;
use PHPUnit\Framework\TestCase;

final class InvalidTokenExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $e = new InvalidTokenException();

        $errors = $e->getErrors();
        $error = $errors[0];

        $this->assertSame(401, $error->getStatus());
        $this->assertSame("Invalid authentication token.", $error->getTitle());
        $this->assertSame("Authentication token introspection failed.", $error->getDetail());
    }
}
