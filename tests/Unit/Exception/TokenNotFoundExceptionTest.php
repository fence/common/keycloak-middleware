<?php

namespace Glance\KeycloakMiddleware\Unit\Exception;

use Glance\KeycloakMiddleware\Exception\TokenNotFoundException;
use PHPUnit\Framework\TestCase;

final class TokenNotFoundExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $e = new TokenNotFoundException();

        $errors = $e->getErrors();
        $error = $errors[0];

        $this->assertSame(401, $error->getStatus());
        $this->assertSame("Invalid authentication token.", $error->getTitle());
        $this->assertSame("Authentication token not found.", $error->getDetail());
    }
}
