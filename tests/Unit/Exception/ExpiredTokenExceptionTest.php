<?php

namespace Glance\KeycloakMiddleware\Unit\Exception;

use Glance\KeycloakMiddleware\Exception\ExpiredTokenException;
use PHPUnit\Framework\TestCase;

final class ExpiredTokenExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $e = new ExpiredTokenException();

        $errors = $e->getErrors();
        $error = $errors[0];

        $this->assertSame(401, $error->getStatus());
        $this->assertSame("Invalid authentication token.", $error->getTitle());
        $this->assertSame("Authentication token expired.", $error->getDetail());
    }
}
