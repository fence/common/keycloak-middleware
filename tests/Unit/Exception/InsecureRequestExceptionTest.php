<?php

namespace Glance\KeycloakMiddleware\Unit\Exception;

use Glance\KeycloakMiddleware\Exception\InsecureRequestException;
use PHPUnit\Framework\TestCase;

final class InsecureRequestExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $e = new InsecureRequestException();

        $errors = $e->getErrors();
        $error = $errors[0];

        $this->assertSame(401, $error->getStatus());
        $this->assertSame("Invalid scheme.", $error->getTitle());
        $this->assertSame("Insecure request. Please use HTTPS.", $error->getDetail());
    }
}
