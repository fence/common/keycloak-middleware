<?php

namespace Glance\KeycloakMiddleware\Test\Unit;

use Glance\CernAuthentication\User;
use Glance\KeycloakMiddleware\Exception\PermissionDeniedException;
use Glance\KeycloakMiddleware\RoleAuthorizationMiddleware;
use Nyholm\Psr7\Factory\Psr17Factory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Server\RequestHandlerInterface;

final class RoleAuthorizationMiddlewareTest extends TestCase
{
    /** @dataProvider anyOfRolesProvider */
    public function testAnyOfRoles(array $userRoles, array $roles, $shouldPass): void
    {
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://app.cern.ch/api/members")
                            ->withHeader("Authorization", "Bearer test.token")
                            ->withAttribute("keycloak-user", $this->userWithRoles($userRoles));

        $handler = $this->mockRequestHandler();

        $middleware = RoleAuthorizationMiddleware::anyOfRoles($roles);

        if ($shouldPass) {
            $response = $middleware->process($request, $handler);
            $this->assertSame(200, $response->getStatusCode());
        } else {
            $this->expectException(PermissionDeniedException::class);
            $middleware->process($request, $handler);
        }
    }

    public function anyOfRolesProvider(): array
    {
        return [
            [ ["admin"],           ["admin", "super-user"],  true ],
            [ ["member", "admin"], ["admin", "super-user"],  true ],
            [ ["member", "other"], ["admin", "super-user"], false ],
            [ [],                  ["admin", "super-user"], false ],
            [ ["admin"],           ["admin"],                true ],
            [ ["member", "admin"], ["admin"],                true ],
            [ ["member", "other"], ["admin"],               false ],
            [ [],                  ["admin"],               false ],
            [ ["member"],          [],                       true ],
            [ [],                  [],                       true ],
        ];
    }

    /** @dataProvider allOfRolesProvider */
    public function testAllOfRoles(array $userRoles, array $roles, bool $shouldPass): void
    {
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://app.cern.ch/api/members")
                            ->withHeader("Authorization", "Bearer test.token")
                            ->withAttribute("keycloak-user", $this->userWithRoles($userRoles));

        $handler = $this->mockRequestHandler();

        $middleware = RoleAuthorizationMiddleware::allOfRoles($roles);

        if ($shouldPass) {
            $response = $middleware->process($request, $handler);
            $this->assertSame(200, $response->getStatusCode());
        } else {
            $this->expectException(PermissionDeniedException::class);
            $middleware->process($request, $handler);
        }
    }

    public function allOfRolesProvider(): array
    {
        return [
            [ [],                                [],                       true ],
            [ [],                                ["admin"],               false ],
            [ ["member"],                        ["admin"],               false ],
            [ ["admin"],                         ["admin"],                true ],
            [ ["member", "admin"],               ["admin"],                true ],
            [ ["member", "other"],               ["admin"],               false ],
            [ [],                                ["admin", "super-user"], false ],
            [ ["admin"],                         ["admin", "super-user"], false ],
            [ ["member", "admin"],               ["admin", "super-user"], false ],
            [ ["admin", "super-user"],           ["admin", "super-user"],  true ],
            [ ["admin", "super-user", "member"], ["admin", "super-user"],  true ],
        ];
    }

    /** @dataProvider mustHaveRoleProvider */
    public function testMustHaveRole(array $userRoles, bool $shouldPass): void
    {
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://app.cern.ch/api/members")
                            ->withHeader("Authorization", "Bearer test.token")
                            ->withAttribute("keycloak-user", $this->userWithRoles($userRoles));

        $handler = $this->mockRequestHandler();

        $middleware = RoleAuthorizationMiddleware::mustHaveRole("admin");

        if ($shouldPass) {
            $response = $middleware->process($request, $handler);
            $this->assertSame(200, $response->getStatusCode());
        } else {
            $this->expectException(PermissionDeniedException::class);
            $middleware->process($request, $handler);
        }
    }

    public function mustHaveRoleProvider(): array
    {
        return [
            [ ["admin"],            true ],
            [ ["member", "admin"],  true ],
            [ [],                  false ],
            [ ["member"],          false ],
        ];
    }

    public function testNotAuthenticatedRequest(): void
    {
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://app.cern.ch/api/members")
                            ->withHeader("Authorization", "Bearer test.token");

        $handler = $this->mockRequestHandler();

        $middleware = RoleAuthorizationMiddleware::mustHaveRole("admin");

        $this->expectExceptionMessage(
            "Missing 'keycloak-user' attribute. Is this an authenticated route?"
        );
        $middleware->process($request, $handler);
    }

    public function testInvalidUserAttribute(): void
    {
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://app.cern.ch/api/members")
                            ->withHeader("Authorization", "Bearer test.token")
                            ->withAttribute("keycloak-user", "wrong-user");

        $handler = $this->mockRequestHandler();

        $middleware = RoleAuthorizationMiddleware::mustHaveRole("admin");

        $userClass = User::class;
        $this->expectExceptionMessage(
            "Attribute 'keycloak-user' must be of the class: '{$userClass}'"
        );
        $middleware->process($request, $handler);
    }

    private function mockRequestHandler(): MockObject
    {
        $factory = new Psr17Factory();
        $response = $factory->createResponse();

        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willReturn($response);

        return $handler;
    }

    private function userWithRoles(array $roles): User
    {
        return User::fromArray([
            "cern_person_id" => 837034,
            "preferred_username"       => "mgunters",
            "email"          => "mario.simao@cern.ch",
            "given_name"     => "Mario",
            "family_name"    => "Gunter Simao",
            "name"           => "Mario Gunter Simao",
            "cern_roles"     => $roles,
        ]);
    }
}
