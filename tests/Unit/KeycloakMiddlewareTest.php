<?php

namespace Glance\KeycloakMiddleware\Test\Unit;

use Glance\CernAuthentication\Exception\FailedToDecodeTokenException;
use Glance\CernAuthentication\Exception\InactiveTokenException;
use Glance\CernAuthentication\KeycloakProvider;
use Glance\CernAuthentication\User;
use Glance\KeycloakMiddleware\Exception\ExpiredTokenException;
use Glance\KeycloakMiddleware\Exception\InsecureRequestException;
use Glance\KeycloakMiddleware\Exception\InvalidTokenException;
use Glance\KeycloakMiddleware\Exception\TokenNotFoundException;
use Glance\KeycloakMiddleware\KeycloakMiddleware;
use Nyholm\Psr7\Factory\Psr17Factory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class KeycloakMiddlewareTest extends TestCase
{
    public function testCreate(): void
    {
        $clientId = "my-app";
        $clientSecret = "supersecret";
        $paths = ["/"];
        $passThrough = ["/public"];

        $middleware = KeycloakMiddleware::create(
            $clientId,
            $clientSecret,
            $paths,
            $passThrough
        );

        $this->assertSame($clientId, $middleware->clientId());
        $this->assertSame($clientSecret, $middleware->clientSecret());
        $this->assertSame($paths, $middleware->authenticatedPaths());
        $this->assertSame($passThrough, $middleware->passThroughPaths());
    }

    public function testInsecureRequest(): void
    {
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "http://app.cern.ch/api/members");

        $handler = $this->mockRequestHandler();

        $middleware = KeycloakMiddleware::create("my-app", "supersecret");

        $this->expectException(InsecureRequestException::class);
        $middleware->process($request, $handler);
    }

    public function testLocalhostRequest(): void
    {
        $this->markTestSkipped(
            "This test currently fails due to an inconsistency between the provider and the middleware"
        );
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "http://localhost/api/members")
                           ->withHeader("Authorization", "Bearer test.token");

        $handler = $this->mockRequestHandler();

        $keycloakProvider = $this->createMock(KeycloakProvider::class);
        $keycloakProvider->method("introspectToken");

        $middleware = KeycloakMiddleware::create(
            "my-app",
            "supersecret",
            ["/"],
            [],
            $keycloakProvider
        );

        $response = $middleware->process($request, $handler);

        $this->assertSame("Authentication succeed", (string) $response->getBody());
    }

    public function testRouteNotOnPaths(): void
    {
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://app.cern.ch/api/members");

        $handler = $this->mockRequestHandler();

        $middleware = KeycloakMiddleware::create(
            "my-app",
            "supersecret",
            ["/private"]
        );

        $response = $middleware->process($request, $handler);

        $this->assertSame("Not authenticated", (string) $response->getBody());
    }

    public function testRouteOnPassThrough(): void
    {
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://app.cern.ch/public/test");

        $handler = $this->mockRequestHandler();

        $middleware = KeycloakMiddleware::create(
            "my-app",
            "supersecret",
            ["/"],
            ["/public"]
        );

        $response = $middleware->process($request, $handler);

        $this->assertSame("Not authenticated", (string) $response->getBody());
    }

    public function testNoAuthorizationHeader(): void
    {
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://app.cern.ch/api/members");

        $handler = $this->mockRequestHandler();

        $middleware = KeycloakMiddleware::create("my-app", "supersecret");

        $this->expectException(TokenNotFoundException::class);
        $middleware->process($request, $handler);
    }

    public function testWrongAuthorizationHeaderFormat(): void
    {
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://app.cern.ch/api/members")
                           ->withHeader("Authorization", "token.without.bearer");

        $handler = $this->mockRequestHandler();

        $middleware = KeycloakMiddleware::create("my-app", "supersecret");

        $this->expectException(TokenNotFoundException::class);
        $middleware->process($request, $handler);
    }

    public function testSuccessfulAuthentication(): void
    {
        $this->markTestSkipped(
            "This test currently fails due to an inconsistency between the provider and the middleware"
        );
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://app.cern.ch/api/members")
                           ->withHeader("Authorization", "Bearer test.token");

        $handler = $this->mockRequestHandler();

        $keycloakProvider = $this->createMock(KeycloakProvider::class);
        $keycloakProvider->method("introspectToken");

        $middleware = KeycloakMiddleware::create(
            "my-app",
            "supersecret",
            ["/"],
            [],
            $keycloakProvider
        );

        $response = $middleware->process($request, $handler);

        $this->assertSame("Authentication succeed", (string) $response->getBody());
    }

    public function testInvalidToken(): void
    {
        $this->markTestSkipped(
            "This test currently fails due to an inconsistency between the provider and the middleware"
        );
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://app.cern.ch/api/members")
                           ->withHeader("Authorization", "Bearer test.token");
        $response = $factory->createResponse();

        $handler = $this->mockRequestHandler();

        $keycloakProvider = $this->createMock(KeycloakProvider::class);
        $keycloakProvider->method("introspectToken")
                         ->willThrowException(new FailedToDecodeTokenException($response));

        $middleware = KeycloakMiddleware::create(
            "my-app",
            "supersecret",
            ["/"],
            [],
            $keycloakProvider
        );

        $this->expectException(InvalidTokenException::class);
        $middleware->process($request, $handler);
    }

    public function testExpiredToken(): void
    {
        $this->markTestSkipped(
            "This test currently fails due to an inconsistency between the provider and the middleware"
        );
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://app.cern.ch/api/members")
                           ->withHeader("Authorization", "Bearer test.token");
        $response = $factory->createResponse();

        $handler = $this->mockRequestHandler();

        $keycloakProvider = $this->createMock(KeycloakProvider::class);
        $keycloakProvider->method("introspectToken")
                         ->willThrowException(new InactiveTokenException($response));

        $middleware = KeycloakMiddleware::create(
            "my-app",
            "supersecret",
            ["/"],
            [],
            $keycloakProvider
        );

        $this->expectException(ExpiredTokenException::class);
        $middleware->process($request, $handler);
    }

    public function testAfterAuthenticateCallback(): void
    {
        $this->markTestSkipped(
            "This test currently fails due to an inconsistency between the provider and the middleware"
        );
        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://app.cern.ch/api/members")
                           ->withHeader("Authorization", "Bearer test.token");

        $handler = $this->mockRequestHandler();

        $user = User::fromArray([
            "active"         => true,
            "cern_person_id" => 837034,
            "preferred_username"       => "mgunters",
            "email"          => "mario@cern.ch",
            "given_name"     => "Mario",
            "family_name"    => "Gunter Simao",
            "name"           => "Mario Gunter Simao",
            "cern_roles"     => ["admin", "user"],
        ]);
        $keycloakProvider = $this->createMock(KeycloakProvider::class);
        $keycloakProvider->method("introspectToken")
                         ->willReturn($user);

        $actualUser = null;
        $callback = function (ServerRequestInterface $request, User $user) use (&$actualUser) {
            $actualUser = $user;
        };

        $middleware = KeycloakMiddleware::create(
            "my-app",
            "supersecret",
            ["/"],
            [],
            $keycloakProvider,
            $callback
        );

        $middleware->process($request, $handler);

        $this->assertSame($user, $actualUser);
    }

    private function mockRequestHandler(): MockObject
    {
        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willReturnCallback(
            function (ServerRequestInterface $request) {
                $factory = new Psr17Factory();
                $response = $factory->createResponse();

                if ($request->getAttribute("keycloak-user")) {
                    $body = $factory->createStream("Authentication succeed");

                    return $response->withBody($body);
                }

                $body = $factory->createStream("Not authenticated");
                return $response->withBody($body);
            }
        );

        return $handler;
    }
}
